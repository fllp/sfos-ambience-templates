NAME=?unnamed
PKGNAME=ambience-$(NAME)
VENDOR=fllp
VERSION=1.0
OSVERSION=3.0.0

RPMBUILD=$(HOME)/rpmbuild
PROJDIR=../$(NAME)-sfos-ambience
AMBFILE=$(NAME).ambience
SPECFILE=$(PKGNAME).spec
IMGFILE=$(NAME).jpg

# check:
	# @if [-z test -f $(SPECFILE) ]; then \
	#     @echo -e "NO SPECFILE FOUND!\nExecute 'make templates' to fill template";\
	# fi


rpm: rpm/$(SPECFILE) prep-sources
	@rpmbuild -bb $<
	@cp $(RPMBUILD)/RPMS/noarch/$(PKGNAME)* .
	@rm -rf $(RPMBUILD)/BUILD/*

prep-sources: $(PKGNAME)/
	@cp -r $^/* $(RPMBUILD)/BUILD/

project_dirs:
	@mkdir -p $(PROJDIR)/$(PKGNAME)/images && mkdir -p $(PROJDIR)/rpm

new: project_dirs ambience_file specfile imagefile
	@cp -n LICENSE $(PROJDIR)/$(PKGNAME)
	@sed 's/\?unnamed/$(NAME)/g' Makefile > $(PROJDIR)/Makefile

ambience_file: template.ambience
	@cp -n $< $(PROJDIR)/$(PKGNAME)/$(AMBFILE)
	@sed -i 's/_NAME_/$(NAME)/g' $(PROJDIR)/$(PKGNAME)/$(AMBFILE)

specfile: template.spec
	@cp -n $< $(PROJDIR)/rpm/$(SPECFILE)
	@sed -i 's/_NAME_/$(NAME)/g; s/_PKGNAME_/$(PKGNAME)/g; s/_VERSION_/$(VERSION)/g; s/_VENDOR_/$(VENDOR)/g; s/_OSVERSION_/$(OSVERSION)/g' $(PROJDIR)/rpm/$(SPECFILE)

imagefile: sfos-dummy-bg.png
	@cp -n $< $(PROJDIR)/$(PKGNAME)/images/$(IMGFILE)
