Name:         _PKGNAME_
Summary:      ADD SUMMARY
Version:      _VERSION_
Release:      1
BuildArch:    noarch
Group:        System/GUI/Other
Vendor:       _VENDOR_
Distribution: SailfishOS
Requires:     sailfish-version >= _OSVERSION_
License:      GPLv3

%description
-- ADD DESCRIPTON --


# shorthand for shared path of ambiences
%global ambiences /usr/share/ambience
# 'realname' of ambience (used for image and ambience file)
%global realname _NAME_

%install
install -D -m 644 %{realname}.ambience -t %{buildroot}%{ambiences}/%{name}/
cp -r images/ %{buildroot}%{ambiences}/%{name}/

%files
# package everything inside ambience folder
%{ambiences}/%{name}
# define license file as per standards
%license LICENSE
# setting 'dir' takes care of proper uninstall
%dir %{ambiences}/%{name}

%post
systemctl-user restart ambienced.service

%postun
systemctl-user restart ambienced.service
